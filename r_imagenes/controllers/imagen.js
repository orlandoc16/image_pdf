var Imagen = require('../models/imagen');
var multer = require('multer');
var path = require('path');
  
var storage = multer.diskStorage({
    destination: './public/uploads',
    filename: function(req,file,cb){
      cb(null,file.fieldname + '-' + Date.now() + path.extname(file.originalname));
    }
  });
  //Init upload
  
  var upload = multer({
    storage: storage
  }).single('imagen');
  
exports.imagen_resize = function(req, res)
{
    upload(req, res, (err)=>{
        if(err){
          res.render('index', {
            msg: err
          });
         } else {
          
           var imagen = new Imagen(req.file);
            res.send("\n\n En proceso de conversión y guardado");
         }
     
      });

}