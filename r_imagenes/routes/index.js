var express = require('express');
var router = express.Router();
var imagenController = require('../controllers/imagen');

/* GET home page. */
router.get('/', function(req, res, next) {
  res.render('index', { title: 'Express' });
});

router.post('/upload', imagenController.imagen_resize);

module.exports = router;
