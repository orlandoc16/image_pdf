var express = require('express');
var router = express.Router();
var imagenController = require('../../controllers/api/imagen');

router.post('/', imagenController.imagen_convert);

module.exports = router;