
const pdfkit = require('pdfkit');
const fs = require('fs');
var Jimp = require('jimp');

var Imagen = function(file)
{
    this.file = file;
    console.log(file);

   crear(file.destination,file.filename,file.fieldname);
}

var crear = function(destination,filename,fieldname)
{
    var ori ='portrait';
   
    var jimage =  Jimp.read(destination+"/"+filename) .then(image => {
        var w = image.bitmap.width; // the width of the image
        var h = image.bitmap.height; // the height of the image

        console.log('Dimensión original de la imagen');
        console.log('Ancho '+w, 'Alto '+h);
    
        if(w>h)
        {
            ori = 'landscape';
            if(w>1123)
            {
                image.resize(1123,Jimp.AUTO);
            }  
        }
        else
        {
            if(h>796)
            {
                image.resize( Jimp.AUTO,1123);
            }
        }
         w = image.bitmap.width; // the width of the image
         h = image.bitmap.height; // the height of the image
         console.log('Dimensión resultante de la imagen');
         console.log('Ancho '+w, 'Alto '+h);
     
        image.write(destination+"/"+'nueva.jpg');
    }).catch(err => {
        // Handle an exception.
        console.error('write error: ', err)
      });

        setTimeout(function()
        {
            var pdf = new pdfkit(
                {
                    size :  [796,1123],
                    layout : ori
                });
                console.log('la orientación '+ori);
            pdf.image(destination+"/"+'nueva.jpg',  0, 0);    
            pdf.pipe(fs.createWriteStream(destination+"/"+fieldname + '-' + Date.now() +'.pdf'));
            pdf.end();
        }, 5000);
}

module.exports = Imagen