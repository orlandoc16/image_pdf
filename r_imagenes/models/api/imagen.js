
const pdfkit = require('pdfkit');
const fs = require('fs');
var Jimp = require('jimp');

var Imagen = function(url)
{
    this.url = url;
    console.log(url);

   crear(url);
}

var crear = function(url)
{
    var ori ='portrait';
   
    var jimage =  Jimp.read(url) .then(image => {
        var w = image.bitmap.width; // the width of the image
        var h = image.bitmap.height; // the height of the image

        console.log('Dimensión original de la imagen');
        console.log('Ancho '+w, 'Alto '+h);
    
        if(w>h)
        {
            ori = 'landscape';
            if(w>1123)
            {
                image.resize(1123,Jimp.AUTO);
            }  
        }
        else
        {
            if(h>796)
            {
                image.resize( Jimp.AUTO,1123);
            }
        }
         w = image.bitmap.width; // the width of the image
         h = image.bitmap.height; // the height of the image
         console.log('Dimensión resultante de la imagen');
         console.log('Ancho '+w, 'Alto '+h);
     
        image.write('./public/uploads/url_nueva.jpg');
    }).catch(err => {
        // Handle an exception.
        console.error('write error: ', err)
      });

        setTimeout(function()
        {
            var pdf = new pdfkit(
                {
                    size :  [796,1123],
                    layout : ori
                });
                console.log('la orientación '+ori);
            pdf.image('./public/uploads/url_nueva.jpg',  0, 0);    
            pdf.pipe(fs.createWriteStream('./public/uploads/url' + '-' + Date.now() +'.pdf'));
            pdf.end();
        }, 5000);
}

module.exports = Imagen